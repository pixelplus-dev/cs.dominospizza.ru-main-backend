// попапы
$('body').on('click', '[data-open-popup]', function(e) {
  e.preventDefault();
  var popupId = $(this).data('open-popup');
  if (popupId) {
    $('.popup').addClass('is-closed');
    $('.popup#' + popupId).removeClass('is-closed');
  }
});

$('.popup__close').click(function() {
  $(this).parents('.popup').addClass('is-closed');
});

$('.popup').click(function(e) {
  if (!$(e.target).parents().is('.popup__window')) $(this).addClass('is-closed');
});
