<?php

use yii\db\Migration;

/**
 * Class m180706_212210_start_tables
 */
class m180706_212210_start_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180706_212210_start_tables cannot be reverted.\n";

        return false;
    }


    public function up()
    {
        // 1.   Пользователи (Администратор, Менеджер)
        $this->createTable('{{%user}}', [
            'id' => $this->bigPrimaryKey(),
            'name' => $this->string()->notNull(),
            'second_name' => $this->string()->notNull(),
            'login' => $this->string()->notNull()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'secret_key' => $this->string(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'role' => $this->smallInteger()->notNull()->defaultValue(1),
            'settings' => $this->text(),
        ]);
        // 2.	Таблица «Список ресторанов»
        $this->createTable('{{%restaurant_list}}', [
            'id' => $this->bigPrimaryKey(),
            'name' => $this->string()->notNull(),
        ]);
        // 3.	Таблица «Связь ресторана и пользователя»
        $this->createTable('{{%relations_restaurant_user}}', [
            'id' => $this->bigPrimaryKey(),
            'user_id' => $this->bigInteger()->notNull()->defaultValue(0), // Внешний ключ
            'restaurant_id' => $this->bigInteger()->notNull()->defaultValue(0), // Внешний ключ
        ]);
        // 4.   Параметры
        $this->createTable('{{%params}}', [
            'id' => $this->bigPrimaryKey(),
            'code' => $this->string()->notNull()->unique(),
            'name' => $this->string(),
            'value' => $this->text(),
        ]);
        // 4.   Параметры
        $this->createTable('{{%complaints}}', [
            'id' => $this->bigPrimaryKey(),
            'restaurant_id' => $this->bigInteger()->notNull()->defaultValue(0), // Внешний ключ
            'user_id' => $this->bigInteger()->notNull()->defaultValue(0), // Внешний ключ
            'date_add' => $this->timestamp(),
            'date_creation' => $this->timestamp()->notNull(),
            'phone' => $this->string()->notNull(),
            'name' => $this->string()->notNull(),
            'email' => $this->string(),
            'source' => $this->string(),
            'text_complaint' => $this->text(),
            'solution' => $this->text(),
            'comment' => $this->text(),
        ]);

        // Таблицы связей
        // Состанвой индекс
        $this->createIndex(
            'idx-relations_restaurant_user-user_id',
            'relations_restaurant_user', // куда Имя таблицы
            'user_id' // куда Имя поля
        );
        // add foreign key
        $this->addForeignKey(
            'idx-relations_restaurant_user-user_id',
            'relations_restaurant_user',
            'user_id', // куда
            'user', // откуда
            'id' // что
        );

        // Состанвой индекс
        $this->createIndex(
            'idx-relations_restaurant_user-restaurant_id',
            'relations_restaurant_user', // куда Имя таблицы
            'restaurant_id' // куда Имя поля
        );
        // add foreign key
        $this->addForeignKey(
            'idx-relations_restaurant_user-restaurant_id',
            'relations_restaurant_user',
            'restaurant_id', // куда
            'restaurant_list', // откуда
            'id' // что
        );

        // Состанвой индекс
        $this->createIndex(
            'idx-complaints-restaurant_id',
            'complaints', // куда Имя таблицы
            'restaurant_id' // куда Имя поля
        );
        // add foreign key
        $this->addForeignKey(
            'idx-complaints-restaurant_id',
            'complaints',
            'restaurant_id', // куда
            'restaurant_list', // откуда
            'id' // что
        );

        // Состанвой индекс
        $this->createIndex(
            'idx-complaints-user_id',
            'complaints', // куда Имя таблицы
            'user_id' // куда Имя поля
        );
        // add foreign key
        $this->addForeignKey(
            'idx-complaints-user_id',
            'complaints',
            'user_id', // куда
            'user', // откуда
            'id' // что
        );
    }

    public function down()
    {
        return false;
    }
}
