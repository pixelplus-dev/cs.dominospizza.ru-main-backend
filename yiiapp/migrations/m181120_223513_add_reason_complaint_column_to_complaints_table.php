<?php

use yii\db\Migration;

/**
 * Handles adding reason_complaint to table `complaints`.
 */
class m181120_223513_add_reason_complaint_column_to_complaints_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('complaints', 'reason_complaint', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('complaints', 'reason_complaint');
    }
}
