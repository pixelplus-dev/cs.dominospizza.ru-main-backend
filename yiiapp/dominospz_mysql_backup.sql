-- Adminer 4.6.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `complaints`;
CREATE TABLE `complaints` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `restaurant_id` bigint(20) NOT NULL DEFAULT '0',
  `user_id` bigint(20) NOT NULL DEFAULT '0',
  `date_add` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `date_creation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `phone` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `text_complaint` text,
  `solution` text,
  `comment` text,
  `email` varchar(255) DEFAULT NULL,
  `source` varchar(255) DEFAULT NULL,
  `tiket` bigint(20) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `subtype` varchar(255) DEFAULT NULL,
  `status_request` varchar(255) DEFAULT NULL,
  `give_promocode` varchar(255) DEFAULT NULL,
  `operator_comment` text,
  `operator` varchar(255) DEFAULT NULL,
  `source_number` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx-complaints-restaurant_id` (`restaurant_id`),
  KEY `idx-complaints-user_id` (`user_id`),
  CONSTRAINT `idx-complaints-restaurant_id` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurant_list` (`id`),
  CONSTRAINT `idx-complaints-user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `migration`;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base',	1530919838),
('m180706_212210_start_tables',	1530919919);

DROP TABLE IF EXISTS `params`;
CREATE TABLE `params` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `value` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `params` (`id`, `code`, `name`, `value`) VALUES
(1,	'logo',	'Логотип сайта',	'/i/logo.png'),
(2,	'copyright',	'Копирайт в подвале',	'© 2018 Domino\'s Pizza'),
(3,	'phone_footer',	'Телефон в подвале',	'8-800-888-88-88'),
(4,	'cron_time',	'Интервал запуска импорта файла по крону (день недели)',	'3'),
(5,	'google_link',	'Ссылка на GoogleDrive',	'https://script.google.com/macros/s/AKfycbx94BHPyc4XSjmGgpY3VLMDQWNirjcsa8CTtCb95bz9uKT0s7Y/exec'),
(6,	'pass_new_user',	'Пароль для нового пользователя',	'123qweasdzxc');

DROP TABLE IF EXISTS `relations_restaurant_user`;
CREATE TABLE `relations_restaurant_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL DEFAULT '0',
  `restaurant_id` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx-relations_restaurant_user-user_id` (`user_id`),
  KEY `idx-relations_restaurant_user-restaurant_id` (`restaurant_id`),
  CONSTRAINT `idx-relations_restaurant_user-restaurant_id` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurant_list` (`id`) ON DELETE CASCADE,
  CONSTRAINT `idx-relations_restaurant_user-user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `restaurant_list`;
CREATE TABLE `restaurant_list` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `session`;
CREATE TABLE `session` (
  `id` char(40) NOT NULL,
  `expire` int(11) NOT NULL,
  `data` blob NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `session` (`id`, `expire`, `data`) VALUES
('0qfokm4ubtfthdjsecucdhq9t3',	1850848717,	'__flash|a:0:{}'),
('6af8atof1op07e148pn4rj9c05',	1850848631,	'__flash|a:0:{}__returnUrl|s:1:\"/\";'),
('6k9s9u793g8lj1mitq9hqid1u3',	1850851558,	'__flash|a:0:{}'),
('6m9h76qqqcnbdp5ll5qk3h4cj6',	1850850574,	'__flash|a:0:{}'),
('eqm48ndecdvf5u0s0ae2dt4pv5',	1850850578,	'__flash|a:0:{}__returnUrl|s:1:\"/\";'),
('j2fc6lj0666ovq3n5kgcpqb513',	1850851633,	'__flash|a:0:{}__returnUrl|s:1:\"/\";'),
('le828h0k4thkhmh195vi2t06f0',	1850851522,	'__flash|a:0:{}__returnUrl|s:1:\"/\";'),
('mu1oso7o4brcu463u60dgmadl4',	1850849006,	'__flash|a:0:{}'),
('n8elfpdij3sl95quklokblr0g1',	1850849010,	'__flash|a:0:{}'),
('qj34umpcfnjfqs5poa186cj1e6',	1850851634,	'__flash|a:0:{}');

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `login` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `auth_key` varchar(32) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '1',
  `role` smallint(6) NOT NULL DEFAULT '1',
  `settings` text,
  `secret_key` varchar(255) DEFAULT NULL,
  `export` smallint(6) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `user` (`id`, `name`, `login`, `email`, `auth_key`, `password_hash`, `status`, `role`, `settings`, `secret_key`, `export`) VALUES
(1,	'Алексей ПиксельПлюс',	'pixelplus',	'a.krainov@pixelplus.ru',	'Q_gKeQmFLSuHeqbuFjzLeuGwRwlYnJ9f',	'$2y$13$NOwNxHpAP6KM0mZsDLjjkuyMbxA.b8g1nf6XHr7A23.5vs.NN9euW',	1,	1,	NULL,	NULL,	1),
(2,	'Админ DominosPizza',	'admin',	'admin@dominospizza.ru',	'wlg5LP7hM8-r1mmtdqwJGuxLzOCXVlzB',	'$2y$13$7w1XKnKkCGOdJHaoSHU/quErTbeCBhcTbrzKglF0d1jWggsgt2z3u',	1,	1,	NULL,	NULL,	1);

-- 2018-08-28 21:27:30
