<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Complaints;
use yii\web\HttpException;

/**
 * ComplaintsSearch represents the model behind the search form of `app\models\Complaints`.
 */
class ComplaintsSearch extends Complaints
{
    public $date_add_1 = '';
    public $date_add_2 = '';
    public $restaurant_list = array();

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'restaurant_id', 'user_id'], 'integer'],
            [['date_add', 'date_creation', 'phone', 'name', 'text_complaint', 'solution', 'comment', 'email', 'source'], 'safe'],
            [['date_add_1', 'date_add_2'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $query = Complaints::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }



        // Фильтр по ресторанам

        if(isset($_REQUEST['ajaxType'])){
            if(isset($_REQUEST['restaurant_id'])){
                $_SESSION['filterComplaints']['restaurant_id'] = $_REQUEST['restaurant_id'];
            } else {
                $_SESSION['filterComplaints']['restaurant_id'] = null;
            }
        }


        if(is_array($_SESSION['filterComplaints']['restaurant_id'])){
            $this->restaurant_list = $_SESSION['filterComplaints']['restaurant_id'];
            $query->andFilterWhere(['restaurant_id' => $this->restaurant_list]);
        }

        // Фильтр по датам
        if($this->date_add_1 == ''){
            $this->date_add_1 = $_SESSION['filterComplaints']['date_add_1'];
        } else {
            $_SESSION['filterComplaints']['date_add_1'] = $this->date_add_1;
        }
        if($this->date_add_2 == ''){
            $this->date_add_2 = $_SESSION['filterComplaints']['date_add_2'];
        } else {
            $_SESSION['filterComplaints']['date_add_2'] = $this->date_add_2;
        }
        if($this->date_add_1 != ''){
            $query->andFilterWhere(['>=', 'date_creation', Yii::$app->formatter->asDate($this->date_add_1, 'yyyy-MM-dd')]);
        }
        if($this->date_add_2 != ''){
            $query->andFilterWhere(['<=', 'date_creation', Yii::$app->formatter->asDate($this->date_add_2, 'yyyy-MM-dd')]);
        }

        // Фильтр по пользователям - убираем - чтобы была возможность смотреть с разных аккаунтов.

/*            if(Yii::$app->user->identity->role == User::ROLE_USER){
                $query->andFilterWhere(['user_id' => Yii::$app->user->identity->id]);
            }*/



        // grid filtering conditions


/*        if(isset($_REQUEST['more'])){
            print_r("<pre>");
            print_r($params);
            print_r("</pre>");
            print_r("<pre>");
            print_r($this);
            print_r("</pre>");
            die();
        } else {

        }*/

        $query->andFilterWhere([
            'id' => $this->id,
            //'restaurant_id' => $this->restaurant_id,
            //'user_id' => $this->user_id,
            //'date_add' => $this->date_add,
            //'date_update' => $this->date_update,
        ]);

        $query->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'text_complaint', $this->text_complaint])
            ->andFilterWhere(['like', 'solution', $this->solution])
            ->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'source', $this->source]);

        return $dataProvider;
    }
}
