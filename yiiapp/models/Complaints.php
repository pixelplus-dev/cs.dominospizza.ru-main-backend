<?php

namespace app\models;

use Yii;
use app\modules\admin\models\RestaurantList;

/**
 * This is the model class for table "complaints".
 *
 * @property int $id
 * @property int $restaurant_id
 * @property int $user_id
 * @property string $date_add
 * @property string $date_creation
 * @property string $phone
 * @property string $name
 * @property string $text_complaint
 * @property string $solution
 * @property string $comment
 * @property string $email
 * @property string $source
 * @property int $tiket
 * @property string $type
 * @property string $subtype
 * @property string $status_request
 * @property string $give_promocode
 * @property string $operator
 * @property string $operator_comment
 * @property string $source_number
 * @property string $reason_complaint
 *
 * @property RestaurantList $restaurant
 * @property User $user
 */
class Complaints extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'complaints';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['restaurant_id', 'user_id', 'tiket'], 'integer'],
            //[['date_add', 'phone', 'name'], 'required'],
            [['date_add', 'date_creation'], 'safe'],
            [['text_complaint', 'solution', 'comment', 'operator_comment', 'reason_complaint'], 'string'],
            [['phone', 'name', 'email', 'source', 'type', 'subtype', 'status_request', 'give_promocode', 'operator', 'source_number'], 'string', 'max' => 255],
            [['restaurant_id'], 'exist', 'skipOnError' => true, 'targetClass' => RestaurantList::className(), 'targetAttribute' => ['restaurant_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'restaurant_id' => 'Restaurant ID',
            'restaurantName' => 'Ресторан',
            'user_id' => 'User ID',
            'userFullName' => 'Менеджер',
            'date_add' => 'Date Add',
            'date_creation' => 'Date Update',
            'phone' => 'Phone',
            'name' => 'Name',
            'text_complaint' => 'Text Complaint',
            'solution' => 'Solution',
            'comment' => 'Comment',
            'email' => 'Email',
            'source' => 'Source',
            'tiket' => 'Tiket',
            'type' => 'Type',
            'subtype' => 'Subtype',
            'status_request' => 'Status Request',
            'give_promocode' => 'Give Promocode',
            'operator' => 'Operator',
            'operator_comment' => 'Operator Comment',
            'source_number' => 'Source Number',
            'reason_complaint' => 'Reason of complaint',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurant()
    {
        return $this->hasOne(RestaurantList::className(), ['id' => 'restaurant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    /* Геттер для названия универа */
    public function getRestaurantName() {
        return $this->restaurant->name;
    }
    /* Геттер для названия универа */
    public function getUserFullName() {
        return $this->user->name.' '.$this->user->second_name;
    }
}
