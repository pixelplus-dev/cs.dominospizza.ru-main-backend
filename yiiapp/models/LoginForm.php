<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\modules\admin\models\RelationsRestaurantUser;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
    public $email;
    public $password;
    public $rememberMe = true;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // login and password are both required
            [['email', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'email' => 'Электронная почта',
            'password' => 'Пароль',
            'rememberMe' => 'Запомнить меня на этом компьютере',
        ];
    }
    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Неверный адрес электронной почты или пароль.');
            }
        }
    }

    /**
     * Logs in a user using the provided email and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {

            Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600*24*30 : 0);

            $session = Yii::$app->session;
            $session->open();
            $_SESSION['filterComplaints']['date_add_1'] = date('01.m.Y');
            $_SESSION['filterComplaints']['date_add_2'] = date('d.m.Y');

            // Получаем все привязаные рестораны
            $arRestaurantUser = RelationsRestaurantUser::find()
                ->andwhere(['user_id'=> Yii::$app->user->identity->id])
                ->asArray()->all();

            if(count($arRestaurantUser) > 0){
                foreach ($arRestaurantUser as $index){
                    $_SESSION['filterComplaints']['restaurant_id'][] = $index['restaurant_id'];
                }
            } else {
                $_SESSION['filterComplaints']['restaurant_id'] = array();
            }



            return true;
        }
        return false;
    }

    /**
     * Finds user by [[email]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByEmail($this->email);
        }

        return $this->_user;
    }
}
