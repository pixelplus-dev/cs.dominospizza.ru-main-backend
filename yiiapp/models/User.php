<?php

namespace app\models;

use Yii;
use yii\web\IdentityInterface;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $name
 * @property string $login
 * @property string $email
 * @property string $auth_key
 * @property string $password_hash
 * @property int $status
 * @property int $role
 * @property string $settings
 *
 * @property Complaints[] $complaints
 * @property RelationsRestaurantUser[] $relationsRestaurantUsers
 */
class User extends ActiveRecord implements IdentityInterface
{
    const ROLE_ADMIN = 1;
    const ROLE_USER = 2;
    const STATUS_ACTIVE = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'login', 'email', 'password_hash'], 'required'],
            [['status', 'role'], 'integer'],
            [['settings'], 'string'],
            [['name', 'login', 'email', 'password_hash'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['login'], 'unique'],
            [['email'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'login' => 'Логин',
            'email' => 'Email',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'status' => 'Status',
            'role' => 'Role',
            'settings' => 'Settings',
        ];
    }

    /**
     * Finds an identity by the given ID.
     *
     * @param string|int $id the ID to be looked for
     * @return IdentityInterface|null the identity object that matches the given ID.
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * Finds an identity by the given token.
     *
     * @param string $token the token to be looked for
     * @return IdentityInterface|null the identity object that matches the given token.
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
//        return static::findOne(['access_token' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByEmail($email)
    {

        $user = static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);

        return $user;

    }

    public static function findBylogin($login)
    {
        return static::findOne(['login' => $login]);
    }

    /**
     * @return int|string current user ID
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string current user auth key
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @param string $authKey
     * @return bool if auth key is valid for current user
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function validatePassword($password)
    {

        return \Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public function generateAuthKey()
    {
        $this->auth_key = \Yii::$app->security->generateRandomString();
    }

    public static function findBySecretKey($key)
    {
        if (!static::isSecretKeyExpire($key))
        {
            return null;
        }
        return static::findOne([
            'secret_key' => $key,
        ]);
    }
    /* Хелперы */
    public function generateSecretKey()
    {
        $this->secret_key = Yii::$app->security->generateRandomString().'_'.time();
    }
    public function removeSecretKey()
    {
        $this->secret_key = null;
    }
    public static function isSecretKeyExpire($key)
    {
        if (empty($key))
        {
            return false;
        }
        $expire = Yii::$app->params['secretKeyExpire'];
        $parts = explode('_', $key);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }
    /**
     * Генерирует хеш из введенного пароля и присваивает (при записи) полученное значение полю password_hash таблицы user для
     * нового пользователя.
     * Вызываеться из модели RegForm.
     */
/*    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }*/
    public function beforeSave($insert)
    {

        if (parent::beforeSave($insert)) {

            $oldPass = $this->getOldAttribute('password_hash');
            $NewPass = $this->getAttribute('password_hash');
            
            if ($this->password_hash != '') {
                if($NewPass != $oldPass){

                    if ($insert) {
                        if ($this->password_hash != '') {

                            $this->password_hash = Yii::$app->security->generatePasswordHash($this->password_hash);
                            $this->auth_key = Yii::$app->security->generateRandomString();
                        }
                    }

                    if (!$insert && $this->password_hash != '') {

                        $this->password_hash = Yii::$app->security->generatePasswordHash($this->password_hash);
                        $this->auth_key = Yii::$app->security->generateRandomString();
                    }
                }
            } else {
                $this->password_hash = $oldPass;
            }


            return true;
        }

        return false;
    }


    public function getRoleText()
    {
        if ($this->role == self::ROLE_ADMIN) {
            return 'Администратор';
        } else return 'Менеджер';
    }

    public function getStatusText()
    {
        if ($this->status == 1) {
            return 'Активен';
        } else return 'Неактивен';
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComplaints()
    {
        return $this->hasMany(Complaints::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelationsRestaurantUsers()
    {
        return $this->hasMany(RelationsRestaurantUser::className(), ['user_id' => 'id']);
    }
}
