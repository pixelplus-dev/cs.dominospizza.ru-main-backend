<?php
namespace app\config;

use Yii;
use yii\base\BootstrapInterface;

/*
/* The base class that you use to retrieve the settings from the database
*/

class myparams implements BootstrapInterface {

    private $db;

    public function __construct() {
        $this->db = Yii::$app->db;
    }

    /**
     * Bootstrap method to be called during application bootstrap stage.
     * Loads all the settings into the Yii::$app->params array
     * @param Application $app the application currently running
     */

    public function bootstrap($app) {

        $settings = \app\models\Params::find()->asArray()->all();

        // Now let load the settings into the global params array

        foreach ($settings as $key => $val) {
            if($val['code'] == 'adminEmail'){
                Yii::$app->params['adminEmail'] = $val['value'];
            } else{
                Yii::$app->params['settings'][$val['code']] = $val['value'];
            }

        }
    }

}