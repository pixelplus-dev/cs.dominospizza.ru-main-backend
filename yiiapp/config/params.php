<?php

return [
    'adminEmail' => 'a.krainov@pixelplus.ru',
    'sendFrom' => ['elhowtest@yandex.ru' => $_SERVER['SERVER_NAME']],
    'secretKeyExpire' => 60 * 60,                       // время хранения секретного ключа
    'pagelist' => [20, 50, 100, 500, 1000],
    'arIndexComplaints' => array(
    '0' => 'source_number',
    '1' => 'source',
    '2' => 'restaurant_id',
    '3' => 'user_id',
    '4' => 'date_creation',
    '5' => 'phone',
    '6' => 'name',
    '7' => 'type',
    '8' => 'subtype',
    '9' => 'reason_complaint',
    '10' => 'text_complaint',
    '11' => 'status_request',
    '12' => 'solution',
    '13' => 'give_promocode',
    '14' => 'email',
    '15' => 'operator',
    '16' => 'comment',
    )
];

