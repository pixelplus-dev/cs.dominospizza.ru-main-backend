<?php

namespace app\controllers;

use app\modules\admin\models\Complaints;
use app\modules\admin\models\RelationsRestaurantUser;
use app\modules\admin\models\RestaurantList;
use app\modules\admin\models\User;
use Yii;
use yii\web\Controller;
use yii\httpclient\Client;

class DaemonController extends Controller
{

    public function actionCron()
    {
        $this->layout;
        //cron_time
        if(Yii::$app->params['settings']['cron_time']){
            if(date('N') == Yii::$app->params['settings']['cron_time']) {
                self::actionIndex();
            }
        }
    }
    public function actionIndex()
    {
        $start = microtime(true);
        $resultTime = '';

        $this->layout;


        // Получаем массив из Гугл Докс
        if(Yii::$app->params['settings']['google_link']){
            $client = new Client();
            $response = $client->createRequest()
                ->setUrl(Yii::$app->params['settings']['google_link'])
                ->send();
            if ($response->isOk) {

                $dataGoogle = json_decode($response->content)->result;

                $resultTime = self::importSave($dataGoogle, $start);

                echo $resultTime;
            }
        } else {
            Yii::$app->session->setFlash('error', 'Не указана ссылка на файл GoogleDrive');
        }


    }

    public function importSave($dataGoogle, $start)
    {
        // Получаем массив пользователей
        $arUser = User::find()->asArray()->all();

        // Получаем массив ресторанов
        $arRestaurant = RestaurantList::find()->asArray()->all();


        $arRelationsRestaurantUser = RelationsRestaurantUser::find()->asArray()->all();

        $arLinkUserRestaurant = array();
        foreach ($arRelationsRestaurantUser as $value) {
            $arLinkUserRestaurant[$value['user_id']][] = $value['restaurant_id'];
        }

        // Проверим на наличие новых ресторанов
        $arRestaurantNew = self::RestaurantCheck($dataGoogle, $arRestaurant);

        $arUserNew = self::userCheck($dataGoogle, $arUser, $arRestaurantNew['result']);
        if (count($arRestaurantNew['error']) > 0 || count($arUserNew['error']) > 0) {

            $str = 'Ошибка в импорт файле <br>';
            foreach ($arRestaurantNew['error'] as $item){
                $str.=$item.' <br>';
            }
            foreach ($arUserNew['error'] as $item){
                $str.=$item.' <br>';
            }
            Yii::$app->session->setFlash('error', $str);
        } else {
            //Удаляем текущие данные из системы
            if(Yii::$app->params['settings']['drop_element_import'] == 'Y'){
                self::deleteAllData();
            }

            foreach ($dataGoogle as $key => $dataItem) {
                $arLinkUserRestaurant = self::addNewItem($dataItem, $arUserNew['result'], $arRestaurantNew['result'], $key, $arLinkUserRestaurant);
            }
            return 'Время выполнения скрипта: ' . round(microtime(true) - $start, 4) . ' сек.';
        }

    }

    public function deleteAllData(){
        Complaints::deleteAll();
    }
    public function userCheck($dataGoogle, $arUser, $arRestaurant)
    {
        $errorResult = array();

        $lastUser = $arUser[count($arUser) - 1]['id'];

        $userFaild = array_search('user_id', Yii::$app->params['arIndexComplaints']);
        $restaurantFaild = array_search('restaurant_id', Yii::$app->params['arIndexComplaints']);

        $result = $arUser;
        $resultUserTemp = array();

        // Получаем список пользователей.
        foreach ($dataGoogle as $key => $item) {

            if ($item[$userFaild] != '') {
                $resultTemp = array();
                // Получаем ID пользователя
                $keyUser = array_search($item[$userFaild], array_column($resultUserTemp, 'name'));

                if ($keyUser === false) {
                    $resultTemp['name'] = $item[$userFaild];
                    $keyRestaurant = array_search($item[$restaurantFaild], array_column($arRestaurant, 'name'));
                    $resultTemp['restaurant_id'][] = $keyRestaurant;
                    $resultUserTemp[] = $resultTemp;
                } else {
                    $keyRestaurant = array_search($item[$restaurantFaild], array_column($arRestaurant, 'name'));
                    $resultUserTemp[$keyUser]['restaurant_id'][] = $keyRestaurant;
                }
            } else {
                $errorResult[] = 'Не указано имя Менеджера в строке ' . ($key + 2);
            }
        }

        foreach ($resultUserTemp as $user) {
            if($user['name'] != ''){
                $keyUser = array_search($user['name'], array_column($result, 'name'));
                if ($keyUser === false) {
                    $resultTemp['id'] = User::addUserExport($user['name'], $lastUser, array_unique($user['restaurant_id']));
                    $resultTemp['name'] = $user['name'];
                    $result[] = $resultTemp;
                    $lastUser++;
                }
            }
        }

        return ['result' => $result, 'error'=>$errorResult];
    }

    public function RestaurantCheck($dataGoogle, $arRestaurant)
    {
        $errorResult = array();

        $restaurantFaild = array_search('restaurant_id', Yii::$app->params['arIndexComplaints']);



        $result = $arRestaurant;
        foreach ($dataGoogle as $key => $item) {

            if ($item[$restaurantFaild] != '') {
                $resultTemp = array();
                // Получаем ID ресторана или создаем
                $keyRestaurant = array_search($item[$restaurantFaild], array_column($result, 'name'));

                if ($keyRestaurant === false) {
                    $resultTemp['id'] = RestaurantList::addRestaurantExport($item[$restaurantFaild]);
                    $resultTemp['name'] = $item[$restaurantFaild];
                    $result[] = $resultTemp;
                }
            } else {
                $errorResult[] = 'Не указан ресторан в строке '.($key+2);
            }

        }

        return ['result' => $result, 'error'=>$errorResult];
    }

    public function addNewItem($dataItem, $arUser, $arRestaurant, $tiket, $arLinkUserRestaurant)
    {

        /*$hasElement = Complaints::find()
            ->where(['tiket' => $tiket])
            ->asArray()->all();*/

        //if (count($hasElement) == 0) {

        $model = new Complaints();

        foreach (Yii::$app->params['arIndexComplaints'] as $key => $params) {
            if ($params == 'date_creation') {

                $dateFormat = date_create_from_format('j/m/Y', $dataItem[$key]);
                if($dateFormat == ''){
                    $dateFormat = date_create_from_format('j.m.Y', $dataItem[$key]);
                }
                $tmp_dir = date_format($dateFormat, 'Y-m-d H:i:s');
                $model->{$params} = $tmp_dir;

            } elseif ($params == 'comment') {
                $model->{$params} = (string)$dataItem[$key];
            } else {
                $model->{$params} = $dataItem[$key];
            }
        }

        // Получаем ID ресторана или создаем
        $keyRestaurant = array_search($model->restaurant_id, array_column($arRestaurant, 'name'));

        if ($keyRestaurant === false) {
            //$model->restaurant_id = RestaurantList::addRestaurantExport($model->restaurant_id);
        } else {
            $model->restaurant_id = $arRestaurant[$keyRestaurant]['id'];
        }

        // Получаем ID пользователя или создаем
        $keyUser = array_search($model->user_id, array_column($arUser, 'name'));

        if ($keyUser === false) {
            //$model->user_id = User::addUserExport($model->user_id, $lastUser, $model->restaurant_id);
        } else {
            $model->user_id = $arUser[$keyUser]['id'];
        }
        // Проверяем есть ли у пользователя хотя бы 1 ресторан

        if (array_key_exists($model->user_id, $arLinkUserRestaurant)) {
            if (!in_array($model->restaurant_id, $arLinkUserRestaurant[$model->user_id])) {
                // Добавляем связь
                $newlink = new RelationsRestaurantUser();
                $newlink->addItem($model->user_id, $model->restaurant_id);
                $arLinkUserRestaurant[$model->user_id][] = $model->restaurant_id;
            }
        } else {
            // Добавляем связь
            $newlink = new RelationsRestaurantUser();
            $newlink->addItem($model->user_id, $model->restaurant_id);
            $arLinkUserRestaurant[$model->user_id][] = $model->restaurant_id;
        }

		if($model->name == ''){
			$model->name = 'нет имени';
		}
		if($model->phone == ''){
			$model->phone = 'нет телефона';
		}
        $model->tiket = $tiket;
        if (!$model->save()) {
            foreach ($model->errors as $error){
                Yii::$app->session->setFlash('error', implode(",", $error));
            }
        }

        return $arLinkUserRestaurant;
        //}

    }
}

