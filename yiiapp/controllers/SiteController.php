<?php

namespace app\controllers;

use app\models\ComplaintsSearch;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\NotFoundHttpException;
use app\models\SendEmailForm;
use app\models\ResetPasswordForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\base\InvalidArgumentException;
use app\modules\admin\models\Complaints;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout','index', 'sendEmail', 'all-answer', 'addSolution', 'with-answer', 'export'],
                'rules' => [
                    [
                        'actions' => ['logout','index', 'all-answer', 'addSolution', 'with-answer', 'export'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['sendEmail'],
                        'allow' => true,
                        'roles' => [''],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */

    public function actionAllAnswer()
    {
        //return $this->render('index');
/*        if(){
            $this->layout = 'index';
        }
        $this->layout = 'index';*/
        $searchModel = new ComplaintsSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->get());

        /*$dataProvider = new ActiveDataProvider([
            'query' => Item::find(),
            'pagination' => [
                'pageSize' => 3,
            ],
        ]);*/
        if(Yii::$app->request->get() && !Yii::$app->request->post()){
            $dateGet = Yii::$app->request->get();
            if(isset($dateGet['ajaxType'])){
                $this->layout = false;
            }
        }
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'status' => 'all',
        ]);

    }
    public function actionIndex()
    {
        $searchModel = new ComplaintsSearch();


        $dataProvider = $searchModel->search(Yii::$app->request->get());
        $dataProvider->query->andWhere(['or',
            ['=', 'solution' , ''],
            ['IS', 'solution', null]
        ]);
        //$dataProvider->query->andWhere(['=', 'solution' , '']);
        //$dataProvider->query->orWhere(['IS', 'solution', null]);
        //$dataProvider->query->andWhere(['IS', 'solution', null]);

        if(Yii::$app->request->get() && !Yii::$app->request->post()){
            $dateGet = Yii::$app->request->get();
            if(isset($dateGet['ajaxType'])){
                $this->layout = false;
            }
        }

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'status' => 'noanswer',
        ]);
    }
    public function actionWithAnswer()
    {
        $searchModel = new ComplaintsSearch();


        $dataProvider = $searchModel->search(Yii::$app->request->get());
        $dataProvider->query->andWhere(['!=', 'solution' , '']);

        if(Yii::$app->request->get() && !Yii::$app->request->post()){
            $dateGet = Yii::$app->request->get();
            if(isset($dateGet['ajaxType'])){
                $this->layout = false;
            }
        }

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'status' => 'withanswer',
        ]);
    }
    public function actionAddSolution()
    {
        if (Yii::$app->request->isAjax) {
            $status = '';

            $data = Yii::$app->request->get();

            $element = Complaints::findOne($data['id']);
            $element->solution = $data['text'];
            $element->save(false);

            if($data['text'] != ''){
                $status = 'ok';
            } else {
                $status = 'delete';
            }

            Yii::$app->timeZone = Yii::$app->getTimeZone();

            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'status' => $status,
                'time' => date('Y-m-d h:i:s'),
            ];
        }
    }
    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionSendEmail()
    {
        $model = new SendEmailForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                if($model->sendEmail()):
                    Yii::$app->getSession()->setFlash('warning', 'Проверьте емайл.');
                    return $this->goHome();
                else:
                    Yii::$app->getSession()->setFlash('error', 'Нельзя сбросить пароль.');
                endif;
            }
        }
        return $this->render('sendEmail', [
            'model' => $model,
        ]);
    }

    public function actionResetPassword($key)
    {
        try {
            $model = new ResetPasswordForm($key);
        }
        catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate() && $model->resetPassword()) {
                Yii::$app->getSession()->setFlash('warning', 'Пароль изменен.');
                return $this->redirect(['/site/login']);
            }
        }
        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionExport()
    {
        $this->layout = false;
        $model = new Complaints();

        // Заменем имя Менеджеа и ресторана на название
        $arColumns = array();
        foreach (Yii::$app->params['arIndexComplaints'] as $item){
            switch ($item) {
                case 'user_id':
                    $arColumns[] = 'userFullName';
                    break;
                case 'restaurant_id':
                    $arColumns[] = 'restaurantName';
                    break;
                default:
                    $arColumns[] = $item;
                    break;

            }
        }

        $getData = Yii::$app->request->get();
        if(!empty($getData)){
            \moonland\phpexcel\Excel::export(['models' => $model->find()->where(['user_id' => $getData['id']])->all(), 'columns' => $arColumns, 'headers' => $arColumns]);
        } else {
            \moonland\phpexcel\Excel::export(['models' => $model->find()->all(), 'columns' => $arColumns, 'headers' => $arColumns]);
        }


    }
}
