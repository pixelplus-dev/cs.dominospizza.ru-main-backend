<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "relations_restaurant_user".
 *
 * @property int $id
 * @property int $user_id
 * @property int $restaurant_id
 *
 * @property RestaurantList $restaurant
 * @property User $user
 */
class RelationsRestaurantUser extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'relations_restaurant_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'restaurant_id'], 'integer'],
            [['restaurant_id'], 'exist', 'skipOnError' => true, 'targetClass' => RestaurantList::className(), 'targetAttribute' => ['restaurant_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'restaurant_id' => 'Restaurant ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurant()
    {
        return $this->hasOne(RestaurantList::className(), ['id' => 'restaurant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function addItem($user_id, $restaurant_id)
    {
        $model = new RelationsRestaurantUser();
        $model->user_id = $user_id;
        $model->restaurant_id = $restaurant_id;
        $model->save();
    }
}
