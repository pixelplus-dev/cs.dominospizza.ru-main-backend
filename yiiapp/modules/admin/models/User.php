<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $name
 * @property string $login
 * @property string $email
 * @property string $auth_key
 * @property string $password_hash
 * @property int $status
 * @property int $role
 * @property string $settings
 *
 * @property Complaints[] $complaints
 * @property RelationsRestaurantUser[] $relationsRestaurantUsers
 */
class User extends \yii\db\ActiveRecord
{
    const ROLE_ADMIN = 1;
    const ROLE_USER = 2;
    const STATUS_ACTIVE = 1;
    public $restaurant_id;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'login', 'email'], 'required'],
            [['status', 'role', 'export'], 'integer'],
            [['settings'], 'string'],
            [['name', 'login', 'email', 'password_hash'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['login'], 'unique'],
            [['email'], 'unique'],
            [['restaurant_id'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'login' => 'Логин',
            'email' => 'Email',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Пароль',
            'status' => 'Статус',
            'statusText' => 'Статус',
            'export' => 'Разрешить выгрузку в EXEL',
            'exportText' => 'Разрешить выгрузку в EXEL',
            'role' => 'Роль',
            'roleText' => 'Роль',
            'settings' => 'Настройки',
            'restaurant_id' => 'Список ресторанов',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $oldPass = $this->getOldAttribute('password_hash');
            $NewPass = $this->getAttribute('password_hash');

            if ($this->password_hash != '') {
                if ($NewPass != $oldPass) {
                    if ($insert) {
                        if ($this->password_hash != '') {
                            $this->password_hash = Yii::$app->security->generatePasswordHash($this->password_hash);
                            $this->auth_key = Yii::$app->security->generateRandomString();
                        }
                    }
                    if (!$insert && $this->password_hash != '') {
                        $this->password_hash = Yii::$app->security->generatePasswordHash($this->password_hash);
                        $this->auth_key = Yii::$app->security->generateRandomString();
                    }
                }
            } else {
                $this->password_hash = $oldPass;
            }

            return true;
        }
        return false;
    }
    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);

        if(is_array($this->restaurant_id)){
            $dataList = $this->restaurant_id;

            $arRestaurantUser = RelationsRestaurantUser::find()
                ->andwhere(['user_id'=> $this->id])
                ->asArray()->all();

            $currentKey = array();
            foreach ($arRestaurantUser as $index){
                if(!in_array($index['restaurant_id'], $dataList)){
                    $modelRelations = RelationsRestaurantUser::findOne($index['id']);
                    $modelRelations->delete();
                } else {
                    $currentKey[] = $index['restaurant_id'];
                }
            }

            foreach ($dataList as $index){
                
                if(!in_array($index, $currentKey)){
                    $modelRelations = new RelationsRestaurantUser;
                    $modelRelations->user_id = $this->id;
                    $modelRelations->restaurant_id = $index;
                    $modelRelations->save();
                }
            }
            Yii::$app->session->setFlash('success', 'Привязка ресторанов обновлена');
        }
    }

    public function addUserExport($name, $lastUser, $restaurant_id){

        $newNumber = $lastUser+1;
        $newUser = new User();
        $newUser->name = $name;
        $newUser->restaurant_id = $restaurant_id;
        $newUser->login = 'manager'.$newNumber;
        $newUser->email = 'manager'.$newNumber.'@test.ru';
        $newUser->password_hash = 'manager123';
        $newUser->status = '1';
        $newUser->role = '2';
        $newUser->save();

        return $newUser->id;
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComplaints()
    {
        return $this->hasMany(Complaints::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelationsRestaurantUsers()
    {
        return $this->hasMany(RelationsRestaurantUser::className(), ['user_id' => 'id']);
    }

    public function getRoleText()
    {
        if ($this->role == self::ROLE_ADMIN) {
            return 'Администратор';
        } else return 'Менеджер';
    }

    public function getStatusText()
    {
        if ($this->status == 1) {
            return 'Активен';
        } else return 'Неактивен';
    }
    public function getExportText()
    {
        if ($this->export == 1) {
            return 'Да';
        } else return 'Нет';
    }

}
