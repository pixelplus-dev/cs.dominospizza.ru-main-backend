<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "restaurant_list".
 *
 * @property int $id
 * @property string $name
 *
 * @property Complaints[] $complaints
 * @property RelationsRestaurantUser[] $relationsRestaurantUsers
 */
class RestaurantList extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'restaurant_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComplaints()
    {
        return $this->hasMany(Complaints::className(), ['restaurant_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelationsRestaurantUsers()
    {
        return $this->hasMany(RelationsRestaurantUser::className(), ['restaurant_id' => 'id']);
    }

    /**
     * @return int
     */
    public function addRestaurantExport($name)
    {
        $newRestaurant = new RestaurantList();
        $newRestaurant->name = $name;
        $newRestaurant->save();

        return $newRestaurant->id;
    }
}
