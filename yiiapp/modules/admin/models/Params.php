<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "params".
 *
 * @property int $id
 * @property string $code
 * @property string $name
 * @property string $value
 */
class Params extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'params';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code'], 'required'],
            [['value'], 'string'],
            [['code', 'name'], 'string', 'max' => 255],
            [['code'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'CODE',
            'name' => 'Название',
            'value' => 'Значение',
        ];
    }
}
