<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\admin\models\Complaints;

/**
 * ComplaintsSearch represents the model behind the search form of `app\modules\admin\models\Complaints`.
 */
class ComplaintsSearch extends Complaints
{
    /* вычисляемый атрибут */
    public $userFullName;
    public $restaurantName;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'restaurant_id', 'user_id'], 'integer'],
            [['date_add', 'date_creation', 'phone', 'name', 'email', 'subtype', 'text_complaint', 'solution', 'comment'], 'safe'],
            [['userFullName', 'restaurantName'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Complaints::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'defaultOrder'=>[
                'date_creation'=>SORT_DESC
            ],
            'attributes' => [
                'id',
                'userFullName' => [
                    'asc' => ['user.name' => SORT_ASC],
                    'desc' => ['user.name' => SORT_DESC],
                    'label' => 'Менеджер'
                ],
                'restaurantName' => [
                    'asc' => ['restaurant_list.name' => SORT_ASC],
                    'desc' => ['restaurant_list.name' => SORT_DESC],
                    'label' => 'Ресторан'
                ],
                'date_add',
                'date_creation',
                'name',
                'email',
                'phone',
                'text_complaint',
                'solution',
                'subtype',
                'comment',
                'tiket',
                'type',
                'status_request',
                'give_promocode',
                'operator',
                'operator_comment' ,
                'source_number',
                'reason_complaint',
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');

            $query->joinWith(['user_id']);
            $query->joinWith(['restaurant']);

            return $dataProvider;
        }



        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            //'restaurant_id' => $this->restaurant_id,
            //'user_id' => $this->user_id,
            'date_add' => $this->date_add,
            'date_creation' => $this->date_creation,
        ]);

        $query->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'subtype', $this->subtype])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'complaints.name', $this->name])
            ->andFilterWhere(['like', 'text_complaint', $this->text_complaint])
            ->andFilterWhere(['like', 'solution', $this->solution])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        if($this->userFullName){
            $query->joinWith(['user' => function ($q) {
                $q->where('user.name LIKE "%' . $this->userFullName . '%"');
            }]);
        }

        if($this->restaurantName){
            $query->joinWith(['restaurant' => function ($q) {
                $q->where('restaurant_list.name LIKE "%' . $this->restaurantName . '%"');
            }]);
        }

        return $dataProvider;
    }
}
