<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use app\modules\admin\models\ImportForm;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\controllers\DaemonController;

class ImportController extends DefaultController
{

    public function actionIndex()
    {

        $model = new ImportForm();

        if (Yii::$app->request->isPost) {


            $model->file = UploadedFile::getInstance($model, 'file');

			if('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' != $model->file->type && 'application/vnd.ms-excel' != $model->file->type){
			\Yii::$app->getSession()->setFlash('error', 'Неверный формат файла. Нужен .xlsx иди .xls');
			 return $this->render('index', ['model' => $model]);
			}
			
            // Получаем путь файла
            $path_file = '/upload/'. $model->file->baseName . '.' . $model->file->extension;
	
            $workfiles = $_SERVER['DOCUMENT_ROOT'].$path_file;

            if ($model->file && $model->validate()) {
                $model->file->saveAs($workfiles);
                // запустим импорт файла.
                self::importFile($workfiles);
            }
        }

        return $this->render('index', ['model' => $model]);

    }


    public function importFile($pathFile)
    {
        $start = microtime(true);
        $resultTime = '';

        $newDate = array();
        $data = \moonland\phpexcel\Excel::import($pathFile, [
            'isMultipleSheet' => true,
            'setFirstRecordAsKeys' => true,
            'getOnlySheet' => 0,
        ]); // $config is an optional

        $dataNew = array();
        // проверка на количество листов
        foreach ($data as $key => $sheet){

            foreach ($sheet as $key_2 => $element){
                if(is_array($element)){
                    $dataNew = $sheet;
                    break;
                } else {
                    $dataNew = $data;
                    break;
                }
            }

        }

        foreach ($dataNew as $item){

            $new_array = array_diff($item, array(''));
            if(count($new_array) != 0){
                $newDate[] = array_values($item);
            }
        }


        $resultTime = DaemonController::importSave($newDate, $start);

        Yii::$app->session->setFlash('success', $resultTime);
    }
}
