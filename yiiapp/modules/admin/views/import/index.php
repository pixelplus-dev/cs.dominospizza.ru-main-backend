<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;

?>
    <h1>Импорт файла с данными</h1>
    <div class="card">
        <div class="card-body">
            <h3>Запусть импорт из Google Drive</h3>
            <?
            Modal::begin([
                'header' => '<h3>Запусть импорт из Google Drive</h3>',
                'id' => 'google-drive',
                'toggleButton' => ['label' => 'Запусть', 'class' => 'btn btn-primary',
                    'onclick' => "
   $.ajax({
    type     :'POST',
    cache    : false,
    url  : '/daemon/index',
    success  : function(response) {
       $('#google-drive').find('.modal-body').text(response);
    }
    });
   
    return false;",
                ],
            ]);
            echo 'Идет загрузка...';
            Modal::end();
            ?>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <h3>Добавить файл в ручную</h3>

            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

            <?= $form->field($model, 'file')->fileInput() ?>

            <?= Html::submitButton('Отправить', ['class' => 'btn btn-success']) ?>

            <?php ActiveForm::end() ?>
        </div>
    </div>
<?
/*$script = '
$("input[type=file]").bootstrapFileInput();
';

$this->registerJs($script);*/
?>