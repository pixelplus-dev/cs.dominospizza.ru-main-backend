<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\admin\models\RestaurantList;
use app\modules\admin\models\User;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Complaints */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="complaints-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php
    /* Получаем список ресторанов*/
    $RestaurantList = RestaurantList::find()->all();

    $arRestaurant = array();
    foreach ($RestaurantList as $val) {
        $arRestaurant[$val->id] = $val->name;
    }
    echo $form->field($model, 'restaurant_id')->dropDownList($arRestaurant);
    ?>
    <? //= $form->field($model, 'restaurant_id')->textInput() ?>
    <?php
    /* Получаем список Менеджеров*/
    $UserList = User::find()->all();

    $arUser = array();
    foreach ($UserList as $val) {
        $arUser[$val->id] = $val->name;
    }
    echo $form->field($model, 'user_id')->dropDownList($arUser);
    ?>
    <? //= $form->field($model, 'user_id')->textInput() ?>

    <?//= $form->field($model, 'date_creation')->textInput() ?>
    <?= $form->field($model, 'date_creation')->textInput(['value' => date('Y-m-d H:i:s'), 'readonly' => true]) ?>
    <?= $form->field($model, 'date_add')->textInput(['value' => date('Y-m-d H:i:s'), 'readonly' => true]) ?>
    <?//= $form->field($model, 'date_add')->textInput() ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text_complaint')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'solution')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
