<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Complaints */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Список Жалоб', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="complaints-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'restaurant_id',
            'user_id',
            'date_creation',
            'date_add',
            'phone',
            'name',
            'text_complaint:ntext',
            'solution:ntext',
            'comment:ntext',
        ],
    ]) ?>

</div>
