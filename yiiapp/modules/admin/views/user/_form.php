<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\User */
/* @var $form yii\widgets\ActiveForm */

$arRestaurantList = app\modules\admin\models\RestaurantList::find()
    ->asArray()->all();

foreach ($arRestaurantList as $index) {
    $arRestaurantModel[$index['id']] = $index['name'];
}

$arSelectUser = array();
$optionsRestaurant = array();
if (!$model->isNewRecord) {
    $arRestaurantUser = app\modules\admin\models\RelationsRestaurantUser::find()
        ->andwhere(['user_id' => $model->id])
        ->asArray()->all();

    foreach ($arRestaurantUser as $index) {
        //$arSelectUser[] = $index['restaurant_id'];
        $optionsRestaurant[$index['restaurant_id']]['Selected'] = true;
    }
}


?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>


    <? /*<div class="form-group field-user-restaurant_id required">
        <label class="control-label" for="user-restaurant_id">Список ресторанов</label>
        <select multiple="" class="form-control" id="user-restaurant_id" name="User[restaurant_id][]">
            <?foreach ($arRestaurantList as $Restaurant){?>
            <option <?if(in_array($Restaurant['id'],$arSelectUser)){?>selected<?}?> value="<?=$Restaurant['id']?>"><?=$Restaurant['name']?></option>
            <?}?>
        </select>

        <div class="help-block"></div>
    </div>*/ ?>

    <?

    echo $form->field($model, 'restaurant_id[]')
        ->dropDownList($arRestaurantModel,
            [
                'options' => $optionsRestaurant,
                'multiple' => 'multiple',
                'class' => 'form-control',
            ]
        );
    ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>


    <? //= $form->field($model, 'password_hash')->textInput(['maxlength' => true]) ?>
    <div class="form-group field-user-password_hash required">
        <label class="control-label" for="user-password_hash">Пароль</label>
        <input type="text" id="user-password_hash" class="form-control" name="User[password_hash]" value=""
               maxlength="255" aria-required="true">

        <div class="help-block"></div>
    </div>

    <?= $form->field($model, 'role')->dropDownList([User::ROLE_ADMIN => 'Администратор', User::ROLE_USER => 'Пользователь']) ?>
    <? if ($model->isNewRecord) { ?>
        <?= $form->field($model, 'status')->checkbox(['checked ' => '']) ?>
        <?= $form->field($model, 'export')->checkbox(['checked ' => '']) ?>
    <? } else { ?>
        <?= $form->field($model, 'status')->checkbox() ?>
        <?= $form->field($model, 'export')->checkbox() ?>
    <? } ?>


    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
