<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Params */

$this->title = 'Создать параметр';
$this->params['breadcrumbs'][] = ['label' => 'Параметры портала', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="params-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
