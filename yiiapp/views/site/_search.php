<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveField;
use app\modules\admin\models\RestaurantList;
use \app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\PostSearch */
/* @var $form yii\widgets\ActiveForm */

// получаем всех связаных ресторанов
$dataRestaurantID = array();
$dataRestaurantName = array();
$optionsRestaurant = array();
$optionsRestaurantDroplist = array();


$arRestaurantUser = app\modules\admin\models\RelationsRestaurantUser::find()
    ->andwhere(['user_id' => Yii::$app->user->identity->id])
    ->asArray()->all();


foreach ($arRestaurantUser as $index) {
    $dataRestaurantID[] = $index['restaurant_id'];
}

if (Yii::$app->user->identity->role == User::ROLE_ADMIN && count($arRestaurantUser) == 0) {
    $dataRestaurantName = RestaurantList::find()
        ->asArray()->all();
} else {
    $dataRestaurantName = RestaurantList::find()
        ->andwhere(['id' => $dataRestaurantID])
        ->asArray()->all();
}
// Получаем данные сессии
if (is_array($_SESSION['filterComplaints']['restaurant_id'])) {
    foreach ($_SESSION['filterComplaints']['restaurant_id'] as $index) {
        $optionsRestaurant[] = $index;
        $optionsRestaurantDroplist[$index]['Selected'] = true;
    }
}
$arRestaurantList = array();
foreach ($dataRestaurantName as $index) {
    $arRestaurantList[$index['id']] = $index['name'];
}


//print_r("<pre>");
//print_r(count($optionsRestaurantDroplist));
//print_r("</pre>");
?>
<section class="section filter-bg">
    <div class="selector">
        <div class="selector__label">
            Ресторан:
        </div>
        <div class="selector__list">

            <?php $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'post',
                'id' => 'filter-restaurant',
            ]);
            ?>
            <span class="selector__item_all ajax_select_all">
                <img src="/i/logo.png" alt="">
                <span>Выбрать все</span>
            </span>
            <span class="selector__item_all ajax_select_off">
                <img src="/i/logo.png" alt="">
                <span>Отменить выборку</span>
            </span>

            <? if (Yii::$app->user->identity->role == User::ROLE_ADMIN) { ?>
                <?
                echo $form->field($model, 'restaurant_id[]')
                    ->dropDownList($arRestaurantList,
                        [
                            'options' => $optionsRestaurantDroplist,
                            'multiple' => 'multiple',
                            'class' => 'form-control',
                        ]
                    );
                ?>
            <? } else { ?>
                <? foreach ($dataRestaurantName as $itemOne) { ?>
                    <span class="selector__item <? if (in_array($itemOne['id'], $optionsRestaurant)) { ?>select<? } ?>"
                          data-id="<?= $itemOne['id'] ?>">
                <img src="/i/logo.png" alt="">
                <span><?= $itemOne['name'] ?></span>
            </span>
                <? } ?>
            <? } ?>
            <?= Html::submitButton('Искать', ['class' => 'btn btn-primary hidden']) ?>
            <?php ActiveForm::end(); ?>
        </div>
    </div>

</section>

