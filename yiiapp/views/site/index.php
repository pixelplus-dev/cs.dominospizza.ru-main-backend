<?php

/* @var $this yii\web\View */

use yii\widgets\ListView;
use yii\widgets\More;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;
use app\models\User;
use app\models\Complaints;

$this->title = Yii::$app->name;

// Получаем список ресторанов

?>
<?//php \yii\widgets\Pjax::begin() ?>

<?= $this->render('_search', ['model' => $searchModel]) ?>
<?
// Получаем данные о количестве

if (Yii::$app->user->identity->role == User::ROLE_USER) {
    $arComplaints = Complaints::find()
        ->where(['restaurant_id' => $searchModel->restaurant_list])
        //->andwhere(['user_id' => Yii::$app->user->identity->id])
        ->andwhere(['>=', 'date_creation', Yii::$app->formatter->asDate($searchModel->date_add_1, 'yyyy-MM-dd')])
        ->andwhere(['<=', 'date_creation', Yii::$app->formatter->asDate($searchModel->date_add_2, 'yyyy-MM-dd')])
        ->asArray()->all();
} else {
    $arComplaints = Complaints::find()
        ->where(['restaurant_id' => $searchModel->restaurant_list])
        ->andwhere(['>=', 'date_creation', Yii::$app->formatter->asDate($searchModel->date_add_1, 'yyyy-MM-dd')])
        ->andwhere(['<=', 'date_creation', Yii::$app->formatter->asDate($searchModel->date_add_2, 'yyyy-MM-dd')])
        ->asArray()->all();
}


$kol_no_answer = 0;
$kol_with_answer = 0;
$kol_all = count($arComplaints);

foreach ($arComplaints as $item) {
    if ($item['solution'] == '') {
        $kol_no_answer++;
    } else {
        $kol_with_answer++;
    }
}


/*$script = "
$('#period-filter').on('click', function(e) {
    e.preventDefault();
    $.ajax({
       url: ".Yii::$app->controller->route.",
       data: {id: 'test'},
       success: function(data) {
           console.log('test2');
       }
    });
});";*/

$script = '

    $(document).ajaxStart(function () {
        $("#overlay").css("display", "block");
    });

    $(document).ajaxComplete(function () {
        $("#overlay").css("display", "none");
    });


    function $getFilterRestaurant() {
        var arRestaurant = "";
        if ($("form#filter-restaurant #complaintssearch-restaurant_id").length > 0) {

            var arRestaurantSelect = $("form#filter-restaurant #complaintssearch-restaurant_id").val();
            for (var i = 0, l = arRestaurantSelect.length; i < l; i++) {
                arRestaurant += "restaurant_id[]=" + arRestaurantSelect[i] + "&";
            }
        } else {
            $("form#filter-restaurant .selector__item.select").each(function (indx, element) {
                arRestaurant += "restaurant_id[]=" + $(this).data("id") + "&";
            });
        }
        return arRestaurant;
    }

    function ajaxLoad(dataAr) {
        var filterRest = $getFilterRestaurant();
        var finalGetData = dataAr + "&" + filterRest;
        $.ajax({
            data: finalGetData,
            type: "GET",
            success: function (data) {
                $("main.page__content").html(data);
                init();
            }
        });
    }

    $(document).on("click", ".issue__save", function (e) {
        var element = $(this).parents(".issues-list__item");
        var textSolution = element.find("textarea").val();
        var id = $(this).data("id");
        e.preventDefault();
        $.ajax({
            url: "/add-solution",
            data: {text: textSolution, id: id},
            type: "GET",
            success: function (data) {
                if (data.status == "ok") {
                    element.addClass("is-ok");
                }
                if (data.status == "delete") {
                    element.removeClass("is-ok");
                }
                element.find(".data-update").text(data.time);
            }
        });
    });

    $(document).on("change", ".period__input", function (e) {
        //$("#period-filter").submit();
        ajaxLoad($(this).parents("form").serialize());
    });
    $(".period__input").datepicker({
        dateFormat: "dd.mm.yy",
        onSelect: function (dateText) {
            //$("#period-filter").submit();        
            ajaxLoad($(this).parents("form").serialize());
        }
    });

    $(document).on("click", "form#filter-restaurant .selector__item", function () {
        var element = $(this);
        element.toggleClass("select");

        var arRestaurant = [];

        $("form#filter-restaurant .selector__item.select").each(function (indx, element) {
            arRestaurant[indx] = $(this).data("id");
        });

        $.ajax({
            data: {restaurant_id: arRestaurant, ajaxType: true},
            type: "GET",
            success: function (data) {
                $("main.page__content").html(data);
                init();
            }
        });
    });

    function init() {
        $(document).find(".period__input").datepicker({
            dateFormat: "dd.mm.yy",
            onSelect: function (dateText) {
                ajaxLoad($(this).parents("form").serialize());
            }
        });
    }

    $(document).on("change", "form#filter-restaurant #complaintssearch-restaurant_id", function () {
        var element = $(this);
        //element.toggleClass("select");

        var arRestaurant = element.val();

        $.ajax({
            data: {restaurant_id: arRestaurant, ajaxType: true},
            type: "GET",
            success: function (data) {
                $("main.page__content").html(data);
                init();
            }
        });
    });
    $(document).on("click", ".ajax_select_all", function () {

        var arRestaurant = [];

        if ($("form#filter-restaurant #complaintssearch-restaurant_id").length > 0) {
            $("form#filter-restaurant #complaintssearch-restaurant_id option").each(function (indx) {
                arRestaurant[indx] = $(this).val();
            });
        } else {
            $("form#filter-restaurant .selector__item").each(function (indx, element) {
                arRestaurant[indx] = $(this).data("id");
            });
        }

        $.ajax({
            data: {restaurant_id: arRestaurant, ajaxType: true},
            type: "GET",
            success: function (data) {
                $("main.page__content").html(data);
                init();
            }
        });
    });
    
        $(document).on("click", ".ajax_select_off", function () {

        var arRestaurant = [];

        $.ajax({
            data: {restaurant_id: arRestaurant, ajaxType: true},
            type: "GET",
            success: function (data) {
                $("main.page__content").html(data);
                init();
            }
        });
    });
';

$this->registerJs($script);
?>
<script>


</script>
<div id="overlay"></div>

<section class="section">
    <div class="issues-list" rel="#more">
        <?php $form = ActiveForm::begin(['id' => 'period-filter']); ?>
        <div class="issues-list__header">
            <div class="issues-list__filter filter">
                <a href="/" class="filter__item <? if ($status == 'noanswer') { ?>is-active<? } ?>">
                    Жалобы без ответа: <em><?= $kol_no_answer ?></em>
                </a>
                <a href="/with-answer" class="filter__item <? if ($status == 'withanswer') { ?>is-active<? } ?>">
                    Жалобы с ответом: <em><?= $kol_with_answer ?></em>
                </a>
                <a href="/all-answer" class="filter__item <? if ($status == 'all') { ?>is-active<? } ?>">
                    Все жалобы: <em><?= $kol_all ?></em>
                </a>
            </div>
            <div class="issues-list__period period">
                <span class="period__label">Период с:</span>
                <?= yii\jui\DatePicker::widget([
                    'name' => 'ComplaintsSearch[date_add_1]',
                    'value' => $searchModel->date_add_1,
                    'language' => 'ru',
                    'dateFormat' => 'dd.MM.yyyy',
                    'id' => 'date_add_1',
                    'clientOptions' => ['defaultDate' => $searchModel->date_add_1],
                    'options' => ['class' => 'period__input'],
                ]) ?>
                <span class="period__label">по:</span>
                <?= yii\jui\DatePicker::widget([
                    'name' => 'ComplaintsSearch[date_add_2]',
                    'value' => $searchModel->date_add_2,
                    'language' => 'ru',
                    'dateFormat' => 'dd.MM.yyyy',
                    'id' => 'date_add_2',
                    'clientOptions' => ['defaultDate' => $searchModel->date_add_2],
                    'options' => ['class' => 'period__input'],
                ]) ?>
            </div>
        </div>
        <input type="hidden" name="ajaxType" value="Y">
        <?php ActiveForm::end(); ?>


        <?= More::$start; ?>
        <?
        echo ListView::widget([
            'dataProvider' => $dataProvider,
            'emptyText' => 'Результатов не найдено',
            //'filterModel' => $searchModel,
            'itemView' => '_post',
//            'itemOptions' => [
//                'class' => '1',
//                'tag' => 'article',
//            ],
            'options' => [
                'tag' => 'div',
                'class' => 'issues-list__list',
            ],
            'layout' => "{items}", // {pager}
        ]);
        ?>
        <?= More::widget(Yii::$app->request->get('page'), $dataProvider); ?>
        <?= More::$end; ?>
        <?//php \yii\widgets\Pjax::end() ?>
    </div>
</section>