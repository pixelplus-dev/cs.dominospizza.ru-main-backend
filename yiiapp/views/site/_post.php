<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\HtmlPurifier;

//$category = \app\modules\admin\models\Tree::findOne($model->category_id);

$status = '';
if($model->solution != ''){
    $status = 'is-ok';
}
?>
<div class="issues-list__item issue <?=$status?>">
    <div class="issue__props">
        <div class="issue__row">
            <div class="issue__prop-name">Источник:</div>
            <div class="issue__prop-sep"></div>
            <div class="issue__prop-val"><?=$model->source?></div>
        </div>
        <div class="issue__row">
            <div class="issue__prop-name">Ресторан:</div>
            <div class="issue__prop-sep"></div>
            <div class="issue__prop-val"><?=$model->restaurantName?></div>
        </div>
        <div class="issue__row">
            <div class="issue__prop-name">Дата:</div>
            <div class="issue__prop-sep"></div>
            <div class="issue__prop-val"><?=$model->date_creation?></div>
        </div>
        <div class="issue__row">
            <div class="issue__prop-name">Дата изменения:</div>
            <div class="issue__prop-sep"></div>
            <div class="issue__prop-val data-update"><?=$model->date_add?></div>
        </div>
        <div class="issue__row">
            <div class="issue__prop-name">Имя:</div>
            <div class="issue__prop-sep"></div>
            <div class="issue__prop-val"><?=$model->name?></div>
        </div>
        <div class="issue__row">
            <div class="issue__prop-name">Телефон:</div>
            <div class="issue__prop-sep"></div>
            <div class="issue__prop-val"><?=$model->phone?></div>
        </div>
        <div class="issue__row">
            <div class="issue__prop-name">E-mail:</div>
            <div class="issue__prop-sep"></div>
            <div class="issue__prop-val"><?=$model->email?></div>
        </div>
    </div>
    <div class="issue__text">
        <h3>Текст жалобы:</h3>
        <p>
            <?=nl2br($model->text_complaint)?>
        </p>
    </div>
    <div class="issue__answer">
        <h3>Ответ:</h3>
        <textarea><?=$model->solution?></textarea>
        <button class="issue__save button button_red" data-id="<?=$model->id?>">Сохранить</button>
    </div>
</div>
