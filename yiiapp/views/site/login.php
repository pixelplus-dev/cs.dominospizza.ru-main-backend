<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Войти';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="popup popup_login" id="popup-login">
    <div class="popup__window">
        <div class="popup__header">
            Авторизация
        </div>
        <div class="popup__body">
            <?php $form = ActiveForm::begin([
                'id' => 'login-form',
                'options' => ['class' => 'form form_login'],
                'fieldConfig' => [
                    'options' => [
                        'tag' => false,
                    ],
                    //'template' => "{label}\n{input}\n{error}",
                    'labelOptions' => ['class' => ''],
                ],
            ]); ?>

            <div class="form__field input">
            <?= $form->field($model, 'email')->textInput(['autofocus' => true,'class'=>'']) ?>
            </div>
            <div class="form__field input">
            <?= $form->field($model, 'password')->passwordInput(['class'=>'']) ?>
            </div>
            <div class="form__field checkbox">
                <?= $form->field($model, 'rememberMe')->checkbox([
                        'value' => false,
                    'template' => "{input}{label}\n{error}",
                ]) ?>
            </div>
             <?= Html::submitButton('Войти', ['class' => 'form__submit button button_blue button_text_white', 'name' => 'login-button']) ?>
            <?
            echo Html::a('Забыли пароль?',
                Yii::$app->urlManager->createAbsoluteUrl(
                    [
                        '/site/send-email'
                    ]
                ), ['class' => 'form__forgot']);
            ?>
            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>

