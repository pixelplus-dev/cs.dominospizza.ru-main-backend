<?

$this->beginContent('@app/views/layouts/structure.php');

use yii\widgets\Breadcrumbs;
use app\widgets\Alert;

?>
        <?= Alert::widget() ?>
        <?/*= Breadcrumbs::widget([
            'homeLink' => [
                'label' => 'Главная',
                'url' => Yii::$app->homeUrl,
            ],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            'options' => ['class' => 'b-crumbs'],
        ]) */?>
        <? echo $content; ?>
<?
$this->endContent();