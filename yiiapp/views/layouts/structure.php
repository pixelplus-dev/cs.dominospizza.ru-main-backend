<?php

/* @var $this \yii\web\View */

/* @var $content string */


use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use app\assets\AppAsset;
use yii\helpers\Url;
use app\models\User;

AppAsset::register($this);

$session = Yii::$app->session;
$session->open();

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="page">
<?php $this->beginBody() ?>

<header class="page__header header">
    <? if (Yii::$app->user->isGuest) { ?>
        <div class="header__user user">

            <div class="user__avatar">
                <img src="/i/icon-user.svg" alt="">
            </div>
            <div class="user__name">
                <a href="<?=Yii::$app->user->loginUrl?>">Авторизация  </a>
            </div>

        </div>
        <a href="#" class="header__login" data-open-popup="popup-login">
            Войти
        </a>
    <? } else {
        $userName = '';
        if (!Yii::$app->user->isGuest) {
            $userName = Yii::$app->user->identity->name;
        }
        ?>

        <div class="header__user user">
            <div class="user__avatar">
                <img src="/i/icon-user.svg" alt="">
            </div>
            <div class="user__name"><?= $userName ?></div>
        </div>
        <? if (!Yii::$app->user->isGuest) {
            ?>
            <? if (Yii::$app->user->identity->export == '1' || Yii::$app->user->identity->role == User::ROLE_ADMIN) { ?>
                <div class="header__download">
                    <a href="/site/export/">
                        <span>Выгрузить все</span>
                    </a>
                    <a href="/site/export/?id=<?= Yii::$app->user->identity->id ?>">
                        <span>Только свои </span>
                    </a>
                </div>
            <? } ?>
        <? } ?>


        <? if (Yii::$app->user->identity->role == User::ROLE_ADMIN) { ?>
            <a href="/admin/" class="header__login" style="margin-right: 10px">
                Панель управления
            </a>
        <? } ?>

        <?= Html::a("Выход", ['/site/logout'], [
            'data' => ['method' => 'post'],
            'class' => 'header__login',
        ]); ?>

    <? } ?>


</header>
<main class="page__content">
        <?= $content ?>
</main>

<footer class="page__footer footer">
    <div class="footer__copyright">
        <img src="<?=Yii::$app->params['settings']['logo']?>" alt="">
        <span><?=Yii::$app->params['settings']['copyright']?></span>
    </div>
    <a href="tel:<?=Yii::$app->params['settings']['phone_footer']?>" class="footer__phone"><?=Yii::$app->params['settings']['phone_footer']?></a>
</footer>

<?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage() ?>


<?
/*
        <div class="container-fluid page-styling site-header-before">
            <div class="row">
                <div class="col-lg-4">
                    <ul class="links_list links_list-align-left align-center-desktop topbar-social">
                        <li>
                            <p class="links_list-value">
                                <a href="<?=Yii::$app->params['settings']['soc-link-facebook']?>" target="_blank" rel="nofollow">
                                    <i class="fa fa-facebook"></i>
                                </a>
                            </p>
                        </li>
                        <li>
                            <p class="links_list-value">
                                <a href="mailto:<?=Yii::$app->params['settings']['soc-link-email']?>" target="_blank" rel="nofollow">
                                    <i class="fa fa-paper-plane"></i>
                                </a>
                            </p>
                        </li>

                        <li>
                            <p class="links_list-value">
                                <a href="<?=Yii::$app->params['settings']['soc-link-instagram']?>" target="_blank" rel="nofollow">
                                    <i class="fa fa-instagram"></i>
                                </a>
                            </p>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-8">
                    <ul class="links_list links_list-align-right align-center-desktop topbar-contacts">
                        <li>
                            <p class="links_list-label">
                                Адрес
                            </p>
                            <p class="links_list-value">
                                <a href="http://maps.google.com" target="_blank" rel="nofollow"><?=Yii::$app->params['settings']['address-top']?></a>
                            </p>
                        </li>
                        <li>
                            <p class="links_list-label">
                                Контакты
                            </p>
                            <p class="links_list-value">
                                <a href="mailto:<?=Yii::$app->params['settings']['email-top']?>"><?=Yii::$app->params['settings']['email-top']?></a>
                            </p>
                        </li>
                        <li>
                            <p class="links_list-label">
                                Телефон
                            </p>
                            <p class="links_list-value">
                               <?=Yii::$app->params['settings']['phone-top']?>
                            </p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="site-header">
            <p class="h-logo">
                <a href="/"><img src="<?=Yii::$app->params['settings']['logofile']?>" alt="MultiShop"></a>
            </p>
            <div class="h-shop">


                <ul class="h-shop-links">

                    <li class="h-cart">
                        <a class="cart-contents" href="<?=Url::to(['cart/'])?>">
                            <p class="h-cart-icon">
                                <i class="ion-android-cart"></i>
                                <span><?=$session['cart.qty']?$session['cart.qty']:0?></span>
                            </p>
                            <p class="h-cart-total"><?=$session['cart.sum']?$session['cart.sum']:''?></p>
                        </a>
                        <div class="widget_shopping_cart">
                            <div class="widget_shopping_cart_content">
                                <? if (count($session['cart']) > 0) { ?>
                                    <ul class="cart_list">
                                        <? foreach ($_SESSION['cart'] as $key => $cartItem) { ?>
                                            <li>
                                                <a href="#" class="remove del-item" data-id="<?=$key?>">&times;</a>
                                                <a href="<?=Url::to(['catalog/view', 'id'=>$key])?>">
                                                    <img src="<?=$cartItem['img']?>" alt="">
                                                    <?=$cartItem['name']?>
                                                </a>
                                                <span class="quantity"><?=$cartItem['qty']?> &times; <?=number_format($cartItem['price'], 0, ',', ' ') . " руб." ?></span>
                                            </li>
                                        <? } ?>
                                    </ul>
                                    <p class="total"><b>Итого:</b> <?=number_format($session['cart.sum'], 0, ',', ' ') . " руб."?></p>
                                    <p class="buttons">
                                        <a href="<?=Url::to(['cart/'])?>" class="button">Корзина</a>
                                        <a href="/cart/#order" class="button">Оформить</a>
                                    </p>
                                <? } ?>
                            </div>
                        </div>
                    </li>

                    <li class="h-menu-btn" id="h-menu-btn">
                        <i class="ion-navicon"></i> Меню
                    </li>
                </ul>
            </div>
            <div class="mainmenu">

                <nav id="h-menu" class="h-menu">
                    <?php

                    echo Nav::widget([
                        'options' => ['class' => ''],
                        'items' => [
                            ['label' => 'Главная', 'url' => ['/catalog/']],
                            //['label' => 'Сопутка', 'url' => ['/sopytka/']],
                            ['label' => 'Выкуп и комиссия', 'url' => ['/redemption/']],
                            ['label' => 'Доставка и оплата', 'url' => ['/ship-payment/']],
                            ['label' => 'Корзина', 'url' => ['/cart/']],
                            //['label' => 'Отдать', 'url' => ['/']],
                            !Yii::$app->user->isGuest ? (
                            ['label' => 'АдминПанель',
                                'url' => ['/admin'],
                                'options' => [
                                    'class' => 'menu-item-has-children'
                                ],
                                'items' => [
                                    [
                                        'label' => 'Список товаров',
                                        'url' => '/admin/item/'
                                    ],
                                    [
                                        'label' => 'Список заказов',
                                        'url' => '/admin/order/'
                                    ],
                                    [
                                        'label' => 'Список категорий',
                                        'url' => '/admin/tree/'
                                    ],
                                    [
                                        'label' => 'Настройки',
                                        'url' => '/admin/params/'
                                    ],
                                    [
                                        'label' => 'Выйти',
                                        'url' => ['/site/logout']
                                    ],
                                ],

                            ]
                            ) : '',
                        ],
                    ]);
                    ?>
                </nav>
            </div>
        </div>
        <div id="content" class="site-content">
            <div id="primary" class="content-area width-full">
                <main id="main" class="site-main">
                    <?= $content ?>
                </main><!-- #main -->
            </div><!-- #primary -->    </div><!-- #content -->
        <div class="container-fluid blog-sb-widgets page-styling site-footer">
            <div class="row">
                <div class="col-sm-12 col-md-4 widget align-center-tablet f-logo-wrap">
                    <a href="/" class="f-logo">
                        <img src="<?=Yii::$app->params['settings']['logofile']?>" alt="">
                    </a>
                    <p><?=Yii::$app->params['settings']['namesite']?></p>

                </div>
            </div>
        </div>
*/ ?>

