<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ComplaintsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="complaints-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>


    <?= $form->field($model, 'restaurant_id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'date_add') ?>


    <?php // echo $form->field($model, 'phone') ?>

    <?php // echo $form->field($model, 'name') ?>

    <?php // echo $form->field($model, 'text_complaint') ?>

    <?php // echo $form->field($model, 'solution') ?>

    <?php // echo $form->field($model, 'comment') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'source') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
